/*
    Spotify API Token server
    Esta aplicación únicamente toma el CLIENTID y CLIENTSecret
    que brinda spotify, para obtener el token mediante una petición
    POST desde el front-end. 

*/

const express = require('express');
const request = require('request');
const path = require('path');

const app = express();

// http://localhost:3000/spotify/1c0d4d09e6174bc1ac0a691fd009dc34/cba348a4ec7d458ba2c1562b4e9027b4
const URL_API = 'http://localhost:3000/spotify/1c0d4d09e6174bc1ac0a691fd009dc34/cba348a4ec7d458ba2c1562b4e9027b4';
const client_id = '1c0d4d09e6174bc1ac0a691fd009dc34'; // Your client id
const client_secret = 'cba348a4ec7d458ba2c1562b4e9027b4'; // Your secret
const redirect_uri = 'https://accounts.spotify.com/api/token'; // Your redirect uri

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/spotify/:client_id/:client_secret', (req, resp) => {

    let client_id = req.params.client_id;
    let client_secret = req.params.client_secret;
    let spotifyUrl = 'https://accounts.spotify.com/api/token';

    var authOptions = {
        url: spotifyUrl,
        headers: {
            Authorization: 'Basic ' + new Buffer(client_id + ':' + client_secret).toString('base64')
        },
        form: {
            grant_type: 'client_credentials'
        },
        json: true
    };


    request.post(authOptions, (err, httpResponse, body) => {

        if (err) {
            return resp.status(400).json({
                ok: false,
                mensaje: 'No se pudo obtener el token',
                err
            })
        }

        resp.json(body);

    });

});


app.listen(port, (err) => {

    if (err) throw new Error(err);
    console.log(`Servidor corriendo en puerto ${ port }`);
    request(URL_API, function(error, response, body) {
        // console.log('error:', error);
        // console.log('statusCode:', response && response.statusCode);
        // console.log('body:', body);
        body = JSON.parse(body)
        console.log('access_token:', body.access_token); // Print the HTML for the Google homepage.
    });
});